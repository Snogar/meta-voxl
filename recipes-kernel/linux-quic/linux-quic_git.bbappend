# This recipe modifies the kernel build.

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# Include fragment and build list of to merge
# To add more config fragments:
#   - Add the fragment file to the files directory
#   - Add the file name to the SRC_URI, as shown above
#   - Add the file path to the CFG_FRAGMENTS, as shown below

# Modify the config to disable the requirement to sign kernel modules
SRC_URI += "file://disable_MODULE_SIG.cfg"
CFG_FRAGMENTS = "${WORKDIR}/disable_MODULE_SIG.cfg "

# Modify the config to enable UVC Video Support
SRC_URI += "file://enable_UVC_VIDEO.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_UVC_VIDEO.cfg "

# Modify the config to enable USB WWAN Support (WNC Module)
SRC_URI += "file://enable_USB_WWAN.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_USB_WWAN.cfg "

# Include ModalAI patches
SRC_URI += "file://modalAI-WNC-Linux-3.18.patch"
SRC_URI += "file://qcserial.patch"
SRC_URI += "file://sierra.patch"
SRC_URI += "file://i2c-debug.patch"
SRC_URI += "file://uvc.patch"


do_configure_prepend() {
   # We merge the config changes with the default config for the board
   # using merge-config.sh kernel tool

   mergeTool=${S}/scripts/kconfig/merge_config.sh  
   confDir=${S}/arch/${ARCH}/configs
   defconf=${confDir}/${KERNEL_CONFIG}

   ${mergeTool} -m -O ${confDir} ${defconf} ${CFG_FRAGMENTS}

   # The output will be named .config. We rename it back to ${defconf} because
   # that's what the rest of do_configure expects
   mv ${confDir}/.config ${defconf}
   bbnote "Writing back the merged config: ${confDir}/.config to ${defconf}"
}

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

# Set a specific commit hash rather than grabbing the latest
SRCREV = "cb4698366aa625048f3b815af6a0dea8aef9280a"

# Fix the license checksum
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=33fa1116c45f9e8de714033f99edde13"

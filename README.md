meta-voxl
=========

This README file contains information on the contents of the
voxl layer.

Please see the corresponding sections below for details.


Dependencies
============

This layer depends on:

  URI: git://git.openembedded.org/bitbake
  branch: master

  URI: git://git.openembedded.org/openembedded-core
  layers: meta
  branch: master

  A bunch of layers from codeaurora (and other sources)
  meant for Snapdragon builds. See voxl-build for more.


Patches
=======

Please submit any patches against the voxl layer to the
ModalAI mailing list and cc: the maintainer.

Support:    support [at] modalai [dot] com

Maintainer: Rahul Anand <ranand [at] codeaurora [dot] org>


I. Adding the voxl layer to your build
=================================================
Clone this layer and add it to build/conf/bblayers.conf. E.g.
BBLAYERS += "/opt/data/workspace/my-build/poky/meta-voxl"



II. Misc
========

Needless to say that nothing will work unless you have the other open
source layers that go along with this.


